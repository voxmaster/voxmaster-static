# GitLab CI/CD Description:



## To have ability do download from gitlab docker registery:
1. Create deploy token in *Gitlab* **Settings** -> **Repository** -> **Deploy Tokens** ( `✓` `read_registry`) 
I've choose `gitlab-registry-do-k8s` name.
Save secret.

2. Create secret in kubernetes namespace:
```bash
kubectl create secret docker-registry gitlab-registry --docker-server=https://registry.gitlab.com --docker-username=gitlab-registry-do-k8s --docker-password=${DEPLOY_TOKEN_GITLAB_REGISTRY} -n staging
```

3. Add secret to default service account in namespace/ It will be used as default to pull images from gitlab registry
```bash
kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "gitlab-registry"}]}' -n production
```