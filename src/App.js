import React, { useEffect, useState } from 'react';
import sys from './sys.png';
import './App.css';
import * as axios from 'axios';

function App() {
  const [status, setStatus] = useState({});
  const [styles, setStyles] = useState([]);
  const showOld = true;

  useEffect(() => {
    getStatus();
    getBeerStyles();
  }, []);

  const getBeerStyles = async () => {
    try {
      const response = await axios.get(`${process.env.REACT_APP_API_URL}/beer/styles`);
      setStyles(response.data);
    } catch(err) {
      console.log(err);
    }
  }

  const getStatus = async () => {
    try {
      const response = await axios.get(`${process.env.REACT_APP_API_URL}/status`);
      console.log(response.data);
      setStatus(response.data);
    } catch(err) {
      console.log(err);
    }
  }

  return (
    <div className="topContainer">
      {/* <div className="innerContainer">
        {styles.map(
          (style) => {
            return <div>{style.name}</div>
          }
        )}
      </div> */}
      {showOld && <div className="App">
        <header className="App-header">
          <img src={sys} className="App-logo" alt="Buy your sysadmin a beer!" />
          <div className="statusContainer">
            <div><button className="refreshBtn" onClick={getStatus}>Refresh</button></div>
            <div className="statusText">Because server is 
            <b>{status.status ? ' ' + status.status : ' down'}</b>
            {status.status ? null : ' and your admin is having a hard time now'}
            </div>
            <div className="statusText">Backend environment <b>{status.nodeEnv}</b></div>
            <div className="statusText">Backend local hostname <b>{status.hostname}</b></div>
            <div className="statusDivider">Other details</div>
            {Object.keys(status).map((key, i) => {
              return (key === 'status' || key === 'nodeEnv' || key === 'hostname' ? null :
                <div key={'key' + i} className="statusText">{key} <b>{status[key]}</b></div>
              )
            })}
          </div>
        </header>
      </div>}
    </div>
  );
}

export default App;
