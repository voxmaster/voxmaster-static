FROM nginx:latest
COPY ./build/ /usr/share/nginx/html
COPY ./.devops/nginx/default.conf /etc/nginx/conf.d/default.conf